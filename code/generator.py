import keras
from resnet50 import *
import style_transfer_generator as st_g

def build_generator_model(input_shape=(224, 224, 3)):
	#generator= ResNet50(input_shape=input_shape)
	generator = st_g.build_network(input_shape=input_shape)
	print '-------------------------Generator------------------------------'
	print generator.summary()
	
	return generator
