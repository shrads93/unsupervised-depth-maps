import os,math
import shutil
import numpy as np
from robotcar_image import load_image
from robotcar_camera_model import CameraModel
from keras.preprocessing import image as keras_image
import utility as util
import pdb

def generate_data_list(dataPath='../kitti/2012/', keyword='t', fraction_train=0.8):
	dataArr = []
	
	for path,dirs,files in os.walk(dataPath):
		#print path
		for dr in dirs:
			#print dr
			if keyword in dr:
				inpath = os.path.join(dataPath,path[len(dataPath):]) + dr
				#print inpath
				for files in os.walk(inpath):
					for fList in files:					
						for f in fList:
							if '.png' in f:
								readLoc = inpath + '/'+f
								#print readLoc
								dataArr.append(readLoc)

	
	dataArr = np.asarray(dataArr)
	
	np.random.shuffle(dataArr)

	return dataArr[:int(math.ceil(fraction_train*len(dataArr)))], dataArr[int(math.ceil(fraction_train*len(dataArr))):]

def load_images_from_paths(path_arr, grayscale=False, target_size=(224, 224)):
	if grayscale is False:
		model = CameraModel('robotcar_models/', path_arr[0])
	
	img = []
	for filename in path_arr:
		# Getting image
		if grayscale: #depth_map, use keras utility
			im = keras_image.img_to_array(keras_image.load_img(filename, grayscale=grayscale, target_size=target_size))
		else: #robotcar image
			im = load_image(filename, model, target_size=target_size)
		
		img.append(np.asarray(im))

	return np.asarray(img)

def load_path_arrays():
	depth_maps_path12, placeholder = generate_data_list(dataPath='../kitti/2012/', keyword='t', fraction_train=1.0)
	depth_maps_path15, placeholder = generate_data_list(dataPath='../kitti/2015/', keyword='t', fraction_train=1.0)
	
	depth_maps_path = np.concatenate((depth_maps_path12, depth_maps_path15))

	#util.convert_depth_maps_to_RGB_mode(depth_maps_path)
	
	left_images_path_train, left_images_path_test = generate_data_list(dataPath='../robotcar/2015-07-24-14-17-50/stereo/', 
															keyword='left')
	
	# left_images_path_train12, left_images_path_test12 = generate_data_list(dataPath='../kitti2012/training/', keyword='image_0')
	# left_images_path_train15, left_images_path_test15 = generate_data_list(dataPath='../kitti2015/training/', keyword='image_2')

	# left_images_path_train = np.concatenate((left_images_path_train12, left_images_path_train15))
	# left_images_path_test = np.concatenate((left_images_path_test12, left_images_path_test15))

	right_images_path_train = [path.replace('left', 'right') for path in left_images_path_train]
	right_images_path_test = [path.replace('left', 'right') for path in left_images_path_test]

	#save path arrays
	np.save('../path arrays/depth_maps_path.npy', depth_maps_path)
	np.save('../path arrays/left_images_path_train.npy', left_images_path_train)
	np.save('../path arrays/left_images_path_test.npy', left_images_path_test)
	np.save('../path arrays/right_images_path_train.npy', right_images_path_train)
	np.save('../path arrays/right_images_path_test.npy', right_images_path_test)

	#now load directly from saved path arrays
	depth_maps_path = np.load('../path arrays/depth_maps_path.npy')
	left_images_path_train = np.load('../path arrays/left_images_path_train.npy')
	left_images_path_test = np.load('../path arrays/left_images_path_test.npy')
	right_images_path_train = np.load('../path arrays/right_images_path_train.npy')
	right_images_path_test = np.load('../path arrays/right_images_path_test.npy')
	
	return depth_maps_path, left_images_path_train, left_images_path_test, right_images_path_train, right_images_path_test
