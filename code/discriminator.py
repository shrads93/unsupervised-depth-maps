from keras.models import Sequential
from keras.layers import *
from keras.layers.advanced_activations import LeakyReLU
from keras.models import Model

def build_discriminator_model(input_shape=(224, 224, 1)):
	if K.image_data_format() == 'channels_last':
		bn_axis = 3
	else:
		bn_axis = 1

	model = Sequential()
	model.add(Conv2D(64, (4, 4), strides=(2, 2), input_shape=input_shape))
	model.add(LeakyReLU(alpha=0.2))

	model.add(Conv2D(128, (4, 4), strides=(2, 2)))
	#model.add(BatchNormalization(axis=bn_axis))
	model.add(LeakyReLU(alpha=0.2))

	model.add(Conv2D(256, (4, 4), strides=(2, 2)))
	#model.add(BatchNormalization(axis=bn_axis))
	model.add(LeakyReLU(alpha=0.2))

	model.add(Flatten())
	model.add(Dense(1))
	model.add(Activation('sigmoid'))

	'''img_input = Input(shape=input_shape, name="disc_input")
	x = Conv2D(64, (4, 4), strides=(2, 2))(img_input)
	x = LeakyReLU(alpha=0.2)(x)

	x = Conv2D(128, (4, 4), strides=(2, 2))(x)
	x = BatchNormalization(axis=bn_axis)(x)
	x = LeakyReLU(alpha=0.2)(x)

	x = Conv2D(256, (4, 4), strides=(2, 2))(x)
	x = BatchNormalization(axis=bn_axis)(x)
	x = LeakyReLU(alpha=0.2)(x)

	x = Flatten()(x)
	x = Dense(1)(x)
	x = Activation('sigmoid')(x)
	
	

	# Create model.
	model = Model(inputs=img_input, outputs=x, name='discriminator')'''

	print '-------------------------Discriminator------------------------------'
	print model.summary()

	return model

if __name__ == "__main__":
	build_discriminator_model()
	