import numpy as np
from keras.preprocessing import image
import os

def load_images_from_folder(folder_path, grayscale=False, target_size=(224, 224)):
	img = []
	for filename in os.listdir(folder_path):
		if '.png' not in filename: continue
		# Getting image
		im = image.img_to_array(image.load_img((folder_path + filename), grayscale=grayscale, target_size=target_size))
		img.append(np.asarray(im))

	return np.asarray(img)


def load_sample_data(current_folder='../sample_data/'):

	left_images = load_images_from_folder(current_folder+'left/')
	right_images = load_images_from_folder(current_folder+'right/')
	depth_maps = load_images_from_folder(current_folder+'depth/', grayscale=True)

	return left_images, right_images, depth_maps

	
def load_stereo_robotcar(robotcar_folder='../robotcar_data/sample_data/stereo/')
	
	left_images	 = load_images_from_folder(robotcar_folder+'left/')
	right_images = load_images_from_folder(robotcar_folder+'right/')

	return left_images, right_images

def load_depthmap_kitti(kitti2015_folder='../kitti2015/training/'):
	for filename in os.listdir(folder_path):
		print filename

if __name__ == "__main__":
	load_depthmap_kitti()


