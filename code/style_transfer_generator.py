from keras.layers import *
from keras import layers
from keras.models import Model
from keras import backend as K

def build_residual_block(input_tensor, n_filters, block):
	# 3x3 conv
	# batch norm
	# ReLU
	# 3x3 conv
	# Batch norm

	if K.image_data_format() == 'channels_last':
		bn_axis = 3
	else:
		bn_axis = 1

	conv_name_base = 'res' + block
	bn_name_base = 'bn' + block

	#padding by zeros for now as keras doesn't have implementation for reflection/replication padding
	x = Conv2D(n_filters, (3, 3),padding='same',name=conv_name_base + 'a')(input_tensor)
	x = BatchNormalization(axis=bn_axis, name=bn_name_base + 'a')(x)
	x = Activation('relu')(x)

	#padding by zeros for now as keras doesn't have implementation for reflection/replication padding
	x = Conv2D(n_filters, (3, 3),padding='same',name=conv_name_base + 'b')(x)
	x = BatchNormalization(axis=bn_axis, name=bn_name_base + 'b')(x)
	
	x = layers.add([x, input_tensor])

	return x

def build_network(input_tensor=None, input_shape=None):

	if input_tensor is None:
		img_input = Input(shape=input_shape)
	else:
		if not K.is_keras_tensor(input_tensor):
			img_input = Input(tensor=input_tensor, shape=input_shape)
		else:
			img_input = input_tensor
	if K.image_data_format() == 'channels_last':
		bn_axis = 3
	else:
		bn_axis = 1

	#padding by zeros for now as keras doesn't have implementation for reflection/replication padding
	x = Conv2D(32, (9, 9), strides=(1, 1),padding='same', name='conv0')(img_input)
	x = BatchNormalization(axis=bn_axis, name='bn0')(x)
	x = Activation('relu')(x)

	x = Conv2D(64, (3, 3), strides=(2, 2),padding='same', name='conv1')(x) 
	x = BatchNormalization(axis=bn_axis, name='bn1')(x)
	x = Activation('relu')(x)

	x = Conv2D(128, (3, 3), strides=(2, 2), padding='same', name='conv2')(x)
	x = BatchNormalization(axis=bn_axis, name='bn2')(x)
	x = Activation('relu')(x)

	x = build_residual_block(x, 128, "block_1")
	x = build_residual_block(x, 128, "block_2")
	x = build_residual_block(x, 128, "block_3")
	x = build_residual_block(x, 128, "block_4")
	x = build_residual_block(x, 128, "block_5")

	x = Conv2DTranspose(64, (3,3), strides=(2,2), padding='same', name='upconv0')(x)
	x = BatchNormalization(axis=bn_axis, name='bn3')(x)
	x = Activation('relu')(x)

	x = Conv2DTranspose(32, (3,3), strides=(2,2), padding='same', name='upconv1')(x)
	x = BatchNormalization(axis=bn_axis, name='bn4')(x)
	x = Activation('relu')(x)

	#padding by zeros for now as keras doesn't have implementation for reflection/replication padding
	x = Conv2D(1, (9, 9), strides=(1, 1),padding='same',name='conv3')(x)
	x = BatchNormalization(axis=bn_axis, name='bn5')(x)
	x = Activation('relu')(x)

	# Create model.
	model = Model(inputs=img_input, outputs=x, name='style_transfer_generator')

	#print model.summary()
	return model

if __name__ == "__main__":

	style_transfer_g = build_network(input_tensor=None, input_shape=(160, 224, 3))
