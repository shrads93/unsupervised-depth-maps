																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																															# -*- coding: utf-8 -*-
"""ResNet50 model for Keras.

# Reference:

- [Deep Residual Learning for Image Recognition](https://arxiv.org/abs/1512.03385)

Adapted from code contributed by BigMoyan.
"""
# from __future__ import print_function
# from __future__ import absolute_import

import warnings

from keras.layers import *
from keras import layers
from keras.models import Model
from keras import backend as K
from keras.engine.topology import get_source_inputs
from keras.utils import layer_utils
from keras.utils.data_utils import get_file
from keras.applications.imagenet_utils import decode_predictions
from keras.applications.imagenet_utils import preprocess_input
from keras.applications.imagenet_utils import _obtain_input_shape
from scale_layer import Scale

import pdb

def identity_block(input_tensor, kernel_size, filters, stage, block):
	"""The identity block is the block that has no conv layer at shortcut.

	# Arguments
		input_tensor: input tensor
		kernel_size: defualt 3, the kernel size of middle conv layer at main path
		filters: list of integers, the filterss of 3 conv layer at main path
		stage: integer, current stage label, used for generating layer names
		block: 'a','b'..., current block label, used for generating layer names

	# Returns
		Output tensor for the block.
	"""
	filters1, filters2, filters3 = filters
	if K.image_data_format() == 'channels_last':
		bn_axis = 3
	else:
		bn_axis = 1
	conv_name_base = 'res' + str(stage) + block + '_branch'
	bn_name_base = 'bn' + str(stage) + block + '_branch'

	x = Conv2D(filters1, (1, 1), name=conv_name_base + '2a')(input_tensor)
	x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
	x = Activation('relu')(x)

	x = Conv2D(filters2, kernel_size,
			   padding='same', name=conv_name_base + '2b')(x)
	x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
	x = Activation('relu')(x)

	x = Conv2D(filters3, (1, 1), name=conv_name_base + '2c')(x)
	x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

	x = layers.add([x, input_tensor])
	x = Activation('relu')(x)
	return x


def conv_block(input_tensor, kernel_size, filters, stage, block, strides=(2, 2)):
	"""conv_block is the block that has a conv layer at shortcut

	# Arguments
		input_tensor: input tensor
		kernel_size: defualt 3, the kernel size of middle conv layer at main path
		filters: list of integers, the filterss of 3 conv layer at main path
		stage: integer, current stage label, used for generating layer names
		block: 'a','b'..., current block label, used for generating layer names

	# Returns
		Output tensor for the block.

	Note that from stage 3, the first conv layer at main path is with strides=(2,2)
	And the shortcut should have strides=(2,2) as well
	"""
	filters1, filters2, filters3 = filters
	if K.image_data_format() == 'channels_last':
		bn_axis = 3
	else:
		bn_axis = 1
	conv_name_base = 'res' + str(stage) + block + '_branch'
	bn_name_base = 'bn' + str(stage) + block + '_branch'

	x = Conv2D(filters1, (1, 1), strides=strides,
			   name=conv_name_base + '2a')(input_tensor)
	x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
	x = Activation('relu')(x)

	x = Conv2D(filters2, kernel_size, padding='same',
			   name=conv_name_base + '2b')(x)
	x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
	x = Activation('relu')(x)

	x = Conv2D(filters3, (1, 1), name=conv_name_base + '2c')(x)
	x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

	shortcut = Conv2D(filters3, (1, 1), strides=strides,
					  name=conv_name_base + '1')(input_tensor)
	shortcut = BatchNormalization(axis=bn_axis, name=bn_name_base + '1')(shortcut)

	x = layers.add([x, shortcut])
	x = Activation('relu')(x)
	return x


def mid_level_features(x, stage):
	#TODO:add a learnable scale layer
	x = Conv2D(1, (1, 1), name='score_pool' + str(stage))(x)

	return x

def upsampling_block(curr_input, above_input, kernel_size, filter, stage, strides=(1, 1), padding=(1,1)):

	x =  Conv2DTranspose(filter, kernel_size, strides=strides, name='upscore_pool'+str(stage))(curr_input)
	print stage, x.get_shape(), curr_input.get_shape(), above_input.get_shape()
	
	#TODO:scale2x_ layer
	x = layers.add([x, above_input])

	return x

def ResNet50(input_tensor=None, input_shape=None):
	"""Instantiates the ResNet50 architecture.

	Optionally loads weights pre-trained
	on ImageNet. Note that when using TensorFlow,
	for best performance you should set
	`image_data_format="channels_last"` in your Keras config
	at ~/.keras/keras.json.

	The model and the weights are compatible with both
	TensorFlow and Theano. The data format
	convention used by the model is the one
	specified in your Keras config file.

	# Arguments
		include_top: whether to include the fully-connected
			layer at the top of the network.
		weights: one of `None` (random initialization)
			or "imagenet" (pre-training on ImageNet).
		input_tensor: optional Keras tensor (i.e. output of `layers.Input()`)
			to use as image input for the model.
		input_shape: optional shape tuple, only to be specified
			if `include_top` is False (otherwise the input shape
			has to be `(224, 224, 3)` (with `channels_last` data format)
			or `(3, 224, 244)` (with `channels_first` data format).
			It should have exactly 3 inputs channels,
			and width and height should be no smaller than 197.
			E.g. `(200, 200, 3)` would be one valid value.
		pooling: Optional pooling mode for feature extraction
			when `include_top` is `False`.
			- `None` means that the output of the model will be
				the 4D tensor output of the
				last convolutional layer.
			- `avg` means that global average pooling
				will be applied to the output of the
				last convolutional layer, and thus
				the output of the model will be a 2D tensor.
			- `max` means that global max pooling will
				be applied.
		classes: optional number of classes to classify images
			into, only to be specified if `include_top` is True, and
			if no `weights` argument is specified.

	# Returns
		A Keras model instance.

	# Raises
		ValueError: in case of invalid argument for `weights`,
			or invalid input shape.
	"""
	# Determine proper input shape
	input_shape = _obtain_input_shape(input_shape,
									  default_size=224,
									  min_size=197,
									  data_format=K.image_data_format(),
									  include_top=False)

	if input_tensor is None:
		img_input = Input(shape=input_shape)
	else:
		if not K.is_keras_tensor(input_tensor):
			img_input = Input(tensor=input_tensor, shape=input_shape)
		else:
			img_input = input_tensor
	if K.image_data_format() == 'channels_last':
		bn_axis = 3
	else:
		bn_axis = 1

	x = ZeroPadding2D((3, 3))(img_input)
	x = Conv2D(64, (7, 7), strides=(1, 1),name='conv0')(x) #stride changed to (1, 1) from (2, 2)
	x = BatchNormalization(axis=bn_axis, name='bn_conv0')(x)
	x = Activation('relu')(x)
	x = MaxPooling2D((2, 2), strides=(2, 2))(x) #kernel size changed to (2,2) from (3, 3)

	#extra convlution layer
	x = Conv2D(128, (3, 3), strides=(1, 1), padding='same', name='conv1')(x)
	x = BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
	x = Activation('relu')(x) 

	stage_1_out = mid_level_features(x, 1)
	x = MaxPooling2D((2, 2), strides=(2, 2))(x)

	x = conv_block(x, 3, [64, 64, 256], stage=2, block='a', strides=(1, 1))
	x = identity_block(x, 3, [64, 64, 256], stage=2, block='b')
	x = identity_block(x, 3, [64, 64, 256], stage=2, block='c')

	stage_2_out = mid_level_features(x, 2)

	x = conv_block(x, 3, [128, 128, 512], stage=3, block='a')
	x = identity_block(x, 3, [128, 128, 512], stage=3, block='b')
	x = identity_block(x, 3, [128, 128, 512], stage=3, block='c')
	x = identity_block(x, 3, [128, 128, 512], stage=3, block='d')

	stage_3_out = mid_level_features(x, 3)

	x = conv_block(x, 3, [256, 256, 1024], stage=4, block='a')
	x = identity_block(x, 3, [256, 256, 1024], stage=4, block='b')
	x = identity_block(x, 3, [256, 256, 1024], stage=4, block='c')
	x = identity_block(x, 3, [256, 256, 1024], stage=4, block='d')
	x = identity_block(x, 3, [256, 256, 1024], stage=4, block='e')
	x = identity_block(x, 3, [256, 256, 1024], stage=4, block='f')

	stage_4_out = mid_level_features(x, 4)

	x = conv_block(x, 3, [512, 512, 2048], stage=5, block='a')
	x = identity_block(x, 3, [512, 512, 2048], stage=5, block='b')
	x = identity_block(x, 3, [512, 512, 2048], stage=5, block='c')

	#x = AveragePooling2D((7, 7), name='avg_pool')(x)

	stage_5_out = mid_level_features(x, 5) #this doesn't have a scale layer

	x = upsampling_block(stage_5_out, stage_4_out, 2, 1, stage=5, strides=(2, 2))
	x = upsampling_block(x, stage_3_out,2, 1, stage=4, strides=(2, 2))
	x = upsampling_block(x, stage_2_out, 2, 1, stage=3, strides=(2, 2))
	x = upsampling_block(x, stage_1_out, 2, 1, stage=2, strides=(2, 2))

	#upscore_all
	#x = ZeroPadding2D((1, 1))(x) : not needed
	x =  Conv2DTranspose(1, 2, strides=(2,2), name='upscore_all')(x)
	#TODO: scale_2xall layer, after applying scale_2xall layer to x, you should get h_flow

	h_flow = Scale()(x)
	#after applying a Scale layer to h_flow, you will get v_flow
	v_flow = Scale()(h_flow)

	# Create model.
	model = Model(inputs=img_input, outputs=v_flow, name='unsupervised_generator')

	return model
