from keras import backend as K
from keras.engine.topology import InputSpec, Layer
import numpy as np
import tensorflow as tf
import pdb

class Scale(Layer):

	def __init__(self, **kwargs):
		self.input_spec = [InputSpec(ndim=4)]
		super(Scale, self).__init__(**kwargs)

	def build(self, input_shape):
		self.input_spec = [InputSpec(shape=input_shape)]
		# Create a trainable weight variable for this layer.
		self.kernel = self.add_weight(shape=(1,1), name='scale',
									  initializer='uniform',
									  trainable=True)
		super(Scale, self).build(input_shape)  # Be sure to call this somewhere!

	def call(self, x, mask=None):
		return tf.multiply(x, self.kernel)

	def get_config(self):
		config = {'scale': 'Scale layer'}
		base_config = super(Scale, self).get_config()
		return dict(list(base_config.items()) + list(config.items()))

	def compute_output_shape(self, input_shape):
		return input_shape