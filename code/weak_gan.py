from keras import backend as K
from keras import layers
from keras.models import *
from keras.layers.merge import Concatenate
from generator import *
from discriminator import *
import utility as util 
from image_gradient_reg import *
import data_loader
import tensorflow as tf
from warping_layer import *
import random
import math
import pickle
import pdb

def generator_containing_discriminator(generator, discriminator):
	model = Sequential()
	model.add(generator)
	discriminator.trainable = False
	model.add(discriminator)

	return model

def generator_with_warping_supervision(generator):
	right_img_input = Input(shape=(224, 224, 3), name='right_img_input')
	
	h_flow = generator.get_layer('scale_1').output
	v_flow = generator.get_layer('scale_2').output
	
	warping_in = layers.concatenate([right_img_input, h_flow, v_flow], axis=3)# activity_regularizer=smoothness_reg) #TODO
	warping_out = Warping()(warping_in)

	model = Model(inputs=[right_img_input, generator.get_input_at(0)], outputs=warping_out, name="generator_with_warping_supervision")

	return model
	

def build_weak_gan_model():
	generator = build_generator_model()
	discriminator = build_discriminator_model()
	discriminator_on_generator = generator_containing_discriminator(generator, discriminator)
	warping_on_generator = generator_with_warping_supervision(generator)

	#plot networks
	# util.plot_architecture(generator, filepath='../visualize_net/generator.png')
	# util.plot_architecture(discriminator, filepath='../visualize_net/discriminator.png')
	# util.plot_architecture(discriminator_on_generator, filepath='../visualize_net/discriminator_on_generator.png')
	# util.plot_architecture(warping_on_generator, filepath='../visualize_net/warping_on_generator.png')
	
	g_optim = util.get_optimizer('adam')
	d_optim = util.get_optimizer('adam')

	discriminator.compile(loss='binary_crossentropy', optimizer=d_optim, metrics=['accuracy'])
	generator.compile(loss='binary_crossentropy', optimizer=g_optim, metrics=['accuracy'])
	discriminator_on_generator.compile(loss='binary_crossentropy', optimizer=g_optim, metrics=['accuracy'])

	#adding warping as weak supervision mechanism, define loss
	warping_on_generator.compile(loss='mean_squared_error', optimizer=g_optim, metrics=['accuracy'])

	return generator, discriminator, discriminator_on_generator, warping_on_generator


def combine_depth_maps(generated_depth_maps, depth_maps):
	combined = np.concatenate((depth_maps, generated_depth_maps), axis=0)
	labels = [1]*len(depth_maps) + [0]*len(generated_depth_maps)

	return combined, np.array(labels)

def get_depth_map_paths_random(depth_map_paths, batch_size):

	batch_depth_maps = random.sample(depth_map_paths, batch_size)
	return batch_depth_maps

def append_loss_to_dict(l_dict, d_loss, g_loss, warp_loss, d_acc,
						g_acc, warp_acc):
	
	l_dict['d_loss'].append(d_loss)
	l_dict['g_loss'].append(g_loss)
	l_dict['warp_loss'].append(warp_loss)

	l_dict['d_acc'].append(d_acc)
	l_dict['g_acc'].append(g_acc)
	l_dict['warp_acc'].append(warp_acc)

	return l_dict


def train_weak_gan(left_image_paths, right_image_paths, depth_map_paths, generator, discriminator,
					discriminator_on_generator, warping_on_generator, n_epochs=100, batch_size=8):
	
	n_examples = len(left_image_paths)
	n_batches = int(math.ceil(n_examples/batch_size))
	print "num_batches", n_batches

	loss_acc_dict = {'d_loss':[], 'g_loss':[], 'warp_loss':[], 'd_acc':[], 'g_acc':[], 'warp_acc':[]}

	for i in range(n_epochs):
		print 'Epoch: ', i, '/', n_epochs
		d_loss, d_acc = 0, 0
		g_loss, g_acc = 0, 0
		warp_loss, warp_acc = 0, 0

		for j in range(0, n_examples, batch_size):
			batch_images_left = data_loader.load_images_from_paths(left_image_paths[j:j+batch_size])
			batch_images_right = data_loader.load_images_from_paths(right_image_paths[j:j+batch_size])
			batch_depth_maps = data_loader.load_images_from_paths(get_depth_map_paths_random(depth_map_paths, batch_size), 
																	grayscale=True)

			generated_depth_maps = generator.predict(batch_images_left)

			d_depth_maps, labels = combine_depth_maps(generated_depth_maps, batch_depth_maps)

			#Train discriminator on a mix of generated depth maps and ground truth depth maps 
			temp_loss, temp_acc = discriminator.train_on_batch(d_depth_maps, labels)
			d_loss += temp_loss
			d_acc += temp_acc

			print_string = str(j)+ ", Discriminator loss="+str(temp_loss)+" acc="+str(temp_acc)

			#now set discriminator to be non trainable and propagate the loss to generator
			discriminator.trainable = False
			temp_loss, temp_acc = discriminator_on_generator.train_on_batch(batch_images_left, np.array([1]*len(batch_images_left)))
			discriminator.trainable = True
			g_loss += temp_loss
			g_acc += temp_acc

			print_string += ", DG loss="+str(temp_loss)+" acc="+str(temp_acc)

			#apply weak supervision
			temp_loss, temp_acc = warping_on_generator.train_on_batch([batch_images_right, batch_images_left], batch_images_left)
			warp_loss += temp_loss
			warp_acc += temp_acc

			print_string += ", Warp loss="+str(temp_loss)+" acc="+str(temp_acc)

			print print_string
		
		#add loss and accuracy to dictionary
		loss_acc_dict = append_loss_to_dict(loss_acc_dict, d_loss/n_batches, g_loss/n_batches, warp_loss/n_batches,
			d_acc/n_batches, g_acc/n_batches, warp_acc/n_batches)

		pickle.dump(loss_acc_dict, open('../loss_acc_dict.p', 'wb'))
		#save weights
		if i%9 == 0:
			#save weigths
			print "--------epoch= ",i, " saving weights of discriminator and generator to disk------"
			generator.save_weights('../model/generator'+str(i)+'.hdf5', True)
			discriminator.save_weights('../model/discriminator'+str(i)+'.hdf5', True)


	return loss_acc_dict

def test_weak_gan(test_images_path, generator):
	#we don't have ground truth depth maps for robotcar dataset
	#so we will just generate as there is no way to evaluate

	#load weights
	generator.load_weights('../model/generator5.hdf5')

	test_images = data_loader.load_images_from_paths(test_images_path)
	depth_maps = generator.predict(test_images)

	util.save_as_image(filepath='../test_inputs/', images=test_images, grayscale=False)
	util.save_as_image(filepath='../generated_depthmaps/', images=depth_maps, grayscale=True)
	
	return depth_maps

if __name__ == "__main__":
	#load data
	depth_maps_path, left_path_train, right_path_train, left_path_test, right_path_test = data_loader.load_path_arrays()

	#save this test data
	#np.save('../left_path_test.npy', left_path_test)
	#build model
	generator, discriminator, discriminator_on_generator, warping_on_generator = build_weak_gan_model()
	
	#train WGAN
	history = train_weak_gan(left_path_train, right_path_train, depth_maps_path, generator, discriminator, discriminator_on_generator, warping_on_generator, batch_size=1)

	history = pickle.load(open('../loss_acc_dict.p', 'rb'))
	util.plot_result(history, 'acc_loss')
	util.plot_result_loss(history, 'loss')
	#test Generator
	#load test_images
	#left_path_test = np.load('../left_path_test.npy')
	#test_weak_gan(left_path_test[:53], generator)