import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import imread
import caffe
import os, sys
import pdb

def initiallize_net():

	net_model = 'scripts/deploy_resnet50by2_pool.prototxt'
	net_weights = 'model/train_iter_40000.caffemodel'

	phase = caffe.TEST

	if not os.path.exists(net_model):
		print "Please download CaffeNet from Model Zoo before you run this demo"
		return

	net = caffe.Net(net_model, net_weights,phase)

	return net


def get_transformed(image):
	image = image[:,:,[2, 1, 0]] # permute channels from RGB to BGR
	image = np.transpose(image, [1, 0, 2]) #flip width and height
	image = image.astype('float32') #convert from uint8 to single 
	return image


def forward(filepath, blob_name):

	# load a image
	img = imread(filepath)

	t_img = get_transformed(img)
	#plt.imshow(img)

	# set the size of the input (we can skip this if we're happy
	#  with the default; we can also change it later, e.g., for different batch sizes)
	net.blobs[blob_name].reshape(t_img.shape[0], t_img.shape[1], t_img.shape[2], 1)       # batch size  # 3-channel (BGR) images # image size is 227x227

	
	#put data in data blob 
	net.blobs[blob_name].data[...] = t_img

	## perform classification
	net.forward()

	output = net.blobs['h_flow'].data

	return output


if __name__ == '__main__':
	net = initiallize_net()

	filepath = 'images/left/1.jpg'
	predicted_disp = forward(filepath, 'imL')




